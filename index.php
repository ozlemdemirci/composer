<?

    include 'dependencies.php';

    $cr = new MonsterFactory();
    $melfi = $cr->createMonsters('Melfi', -9, 'Tony');
    $livia = $cr->createMonsters('Livia', 11, 'Tony');
    $tony = $cr->createMonsters('Tony', 100, 'none');
    $melfi->speak($melfi->smile());
    $tony->receive_damage($melfi->give_damage());
    $livia->scream($livia->cry());
    $tony->receive_damage($livia->give_damage());


